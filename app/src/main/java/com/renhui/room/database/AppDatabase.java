package com.renhui.room.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.renhui.room.database.daos.BookDao;
import com.renhui.room.database.daos.UserDao;
import com.renhui.room.database.entities.Book;
import com.renhui.room.database.entities.User;

@Database(entities = {User.class, Book.class}, version = 3)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

	public abstract UserDao userDao();

	public abstract BookDao bookDao();

}
