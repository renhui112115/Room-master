# Room-master
Android官方ORM框架Room使用示例

Room由三个重要的组件组成：Database、Entity、DAO。

### Database
Database 是数据库的持有者，是应用持久关联数据的底层连接的主要访问点。而且Database对应的类编写时必须满足下面几个条件：

1. 必须是abstract类而且的extends RoomDatabase。

2. 必须在类头的注释中包含与数据库关联的实体列表(Entity对应的类)。

3. 包含一个具有0个参数的抽象方法，并返回用@Dao注解的类。

在运行时，你可以通过Room.databaseBuilder() 或者 Room.inMemoryDatabaseBuilder()获取Database实例。

### Entity
Entity代表数据库中某个表的实体类。

### DAO
DAO封装用于访问数据库的方法。


### Room 配置方式
在build.gradle中添加如下配置：
```xml
// add for room
implementation "android.arch.persistence.room:runtime:1.1.1"
// room 配合 RxJava
implementation "android.arch.persistence.room:rxjava2:1.1.1"
annotationProcessor 'android.arch.persistence.room:compiler:1.1.1'
// RxJava
implementation 'io.reactivex.rxjava2:rxandroid:2.0.1'
implementation 'io.reactivex.rxjava2:rxjava:2.1.3'
```
